//
//  Extentions.swift
//  gmxInterview
//
//  Created by Harjit Singh on 02/11/19.
//  Copyright © 2019 gmx. All rights reserved.
//

import UIKit


extension UIView {
    //Mark:
    func vibrate(scaleX: CGFloat, scaleY: CGFloat, onAnimationDone: @escaping (() -> ())) {
        UIView.animate(withDuration: 0.05, animations: {
            self.transform = CGAffineTransform(rotationAngle: -0.02).concatenating(CGAffineTransform(scaleX: scaleX, y: scaleY))
            
        }) { (_) in
            UIView.animate(withDuration: 0.05, delay: 0.0, options: [.repeat, .autoreverse], animations: {
                UIView.setAnimationRepeatCount(20)
                self.transform = CGAffineTransform(rotationAngle: 0.02).concatenating(CGAffineTransform(scaleX: scaleX, y: scaleY))
            }, completion: { _ in
                self.transform = CGAffineTransform(rotationAngle: 0).concatenating(CGAffineTransform(scaleX: scaleX, y: scaleY))
                onAnimationDone()
            })
        }
    }
    
    func setBorder(radius:CGFloat, color:UIColor = UIColor.clear) {
         
        self.layer.borderWidth = radius
        self.layer.borderColor = color.withAlphaComponent(0.5).cgColor
        self.clipsToBounds = true
        
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func applyBezierPath(path: UIBezierPath) {
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.masksToBounds = false
        self.layer.mask = maskLayer
    }
    
}


