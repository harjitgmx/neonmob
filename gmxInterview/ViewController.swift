//
//  ViewController.swift
//  gmxInterview
//
//  Created by Harjit Singh on 01/11/19.
//  Copyright © 2019 gmx. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var rootView: UIView!
    @IBOutlet weak var openCloseEnvelopButton: UIButton!
    
    
    
    var arrViews = [UIView]()
    
    let noOfPacksOnEachRow = 4
    let noOfPacksOnEachColumn = 4
    
    lazy var xValue: CGFloat = {
        return (UIScreen.main.bounds.width / CGFloat(self.noOfPacksOnEachRow))
    }()
    
    lazy var yValue: CGFloat = {
        return (UIScreen.main.bounds.height / CGFloat(self.noOfPacksOnEachColumn))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetup()
        viewAnimation()
    }
    
    func viewSetup(){
        let value = Int.random(in: 15...23)
        for i in 0...value {
            let luckPack = PackView(x: 0, y: self.view.bounds.height)
            luckPack.addTarget(self, action: #selector(self.action(sender:)), for: .touchUpInside)
            
            luckPack.tag = i
            
             
            //luckPack.isEnabled = false
            arrViews.append(luckPack)
            self.rootView.addSubview(luckPack)
        }
    }
    
    func viewAnimation(){
        UIView.animate(withDuration: 0.7, delay: 0, options: [.curveEaseOut], animations: {
            var viewY: CGFloat = 20
            var viewX: CGFloat = 0
            
            for (index,vw) in self.arrViews.enumerated() {
                
                vw.frame.origin = CGPoint(x: viewX, y: viewY + CGFloat.random(in: 0.1..<5))
                
                viewX = viewX + self.xValue
                
                if ((index+1) % self.noOfPacksOnEachRow == 0) {
                    viewY = viewY + self.yValue
                    viewX = 0
                }
                
                
                if (index%3 == 0 || index == 0) {
                    vw.transform = CGAffineTransform(rotationAngle: CGFloat(Int.random(in: 1..<10)))
                } else{
                    vw.transform = CGAffineTransform(rotationAngle: CGFloat(-(Int.random(in: 1..<10))))
                }
            }
            
        } )
        
    }
    
    //MARK:Select a card
    @objc func action(sender: UIButton) {
        
        for view in arrViews{
            
            if (view.tag != sender.tag) {
                
                UIView.animate(withDuration: 0.1) {
                    view.isHidden = true
                }
                
            } else {
                
                UIView.animate(withDuration: 0.5, delay: 0.0,options: [.curveEaseIn], animations: {
                    view.transform = .identity
                    view.center  = self.view.center
                    
                }) { (bool) in
                    UIView.animate(withDuration: 0.5,  delay: 0.1,options: [.curveEaseInOut],animations: {
                        view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                    }, completion: { _ in
                        
                        view.vibrate(scaleX: 1.5, scaleY: 1.5, onAnimationDone: {
                            if let packView = view as? PackView {
                                packView.openEnvelop()
                                self.openCloseEnvelopButton.isHidden = false
                            }
                        })
                    })
                }
            }
            
        }
    }
    
    @IBAction func btOpenCloseEnvelop(_ sender: UIButton) {
        for view in arrViews{
              
                if let packView = view as? PackView {
                    packView.CloseEnvelop()
                }
         }
     }
    
    
}


