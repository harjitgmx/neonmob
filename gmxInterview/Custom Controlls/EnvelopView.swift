//
//  EnvelopView.swift
//  gmxInterview
//
//  Created by Harjit Singh on 03/11/19.
//  Copyright © 2019 gmx. All rights reserved.
//

import UIKit
import Foundation

enum EnvelopPhase {
    case top, bottom, left, right
    
    func getBezeirPath(width: CGFloat, height: CGFloat) -> UIBezierPath {
        
        switch self {
        case .top:
            let curveRadius: CGFloat = 10
            let spacing: CGFloat = 3
            
            let bezierPath = UIBezierPath()
            
            bezierPath.move(to: CGPoint(x: spacing, y: 0))
            bezierPath.addLine(to: CGPoint(x: width - spacing, y: 0))
            bezierPath.addLine(to: CGPoint(x: (width / 2) + curveRadius, y: height - curveRadius))
            bezierPath.addCurve(to: CGPoint(x: (width / 2) - curveRadius, y: height - curveRadius), controlPoint1: CGPoint(x: width / 2, y: height - (curveRadius / 2)), controlPoint2: CGPoint(x: width / 2, y: height - (curveRadius / 2)))
            
          //  bezierPath.lineWidth = 1
            bezierPath.close()
            
            return bezierPath
        case .bottom:
            let curveRadius: CGFloat = 10
            let spacing: CGFloat = 3
            
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: spacing, y: height))
            bezierPath.addLine(to: CGPoint(x: width - spacing, y: height))
            bezierPath.addLine(to: CGPoint(x: (width / 2) + curveRadius, y: (curveRadius / 2)))
            bezierPath.addCurve(to: CGPoint(x: (width / 2) - curveRadius, y: (curveRadius / 2)), controlPoint1: CGPoint(x: (width / 2), y: 0), controlPoint2: CGPoint(x: (width / 2), y: 0))
          //  bezierPath.lineWidth = 1
            
            bezierPath.close()
            
            return bezierPath
        case .left:
            
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 0, y: 3))
            bezierPath.addLine(to: CGPoint(x: width, y: height / 3.1))
            bezierPath.addLine(to: CGPoint(x: width, y: height / 1.52))
            bezierPath.addLine(to: CGPoint(x: 0, y: height - 3))
           // bezierPath.lineWidth = 1
            bezierPath.close()
            
            return bezierPath
        case .right:
            
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: width, y: 3))
            bezierPath.addLine(to: CGPoint(x: 0, y: height / 3.1))
            bezierPath.addLine(to: CGPoint(x: 0, y: height / 1.52))
            bezierPath.addLine(to: CGPoint(x: width, y: height - 3))
           // bezierPath.lineWidth = 1
            bezierPath.close()
            
            return bezierPath
        }
    }
    
    func getShadowBezierPath(shadowWidth: CGFloat, width: CGFloat, height: CGFloat) -> UIBezierPath {
        
        // Shadow only for Top,
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: width / 2, y: height - shadowWidth))
        bezierPath.addLine(to: CGPoint(x: width, y: 0))
        bezierPath.addLine(to: CGPoint(x: width - shadowWidth, y: 0))
        bezierPath.addLine(to: CGPoint(x: width / 2, y: height -
        (shadowWidth * 3)))
        bezierPath.addLine(to: CGPoint(x: shadowWidth, y: 0))
        bezierPath.addLine(to: CGPoint(x: 0, y: 0))
        
     //   bezierPath.lineWidth = 1
        bezierPath.close()
        
        return bezierPath
    }
}

class EnvelopView: UIView {
    
    private let envelopFlipViewBackgroundColor = UIColor.white.withAlphaComponent(0.5)
    
    lazy private var topFlipView: UIView = {
        let viewHeight = (self.bounds.height)*35/100
        let viewWidth = self.bounds.width
        
        let view = UIView(frame: CGRect(x: 0, y: 0 - viewHeight / 2, width: viewWidth, height: viewHeight))
        view.backgroundColor = self.envelopFlipViewBackgroundColor
        view.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
        view.isUserInteractionEnabled = false
        
        var logoImageView = UIImageView(image: UIImage(named: "diamond"))
        logoImageView.frame.size = CGSize(width: 30, height: 30)
        logoImageView.center = CGPoint(x: viewWidth / 2, y: viewHeight / 2)
        logoImageView.alpha = 0.4
        logoImageView.contentMode = .scaleAspectFit
        view.addSubview(logoImageView)
        
        let envelopType = EnvelopPhase.top
        
        view.applyBezierPath(path: envelopType.getBezeirPath(width: viewWidth, height: viewHeight))
        
        return view
    }()
    
    lazy private var topFlipShadowView: UIView = {
        let viewHeight = ((self.bounds.height)*35/100) + 5
        let viewWidth = self.bounds.width
        
        let view = UIView(frame: CGRect(x: 0, y: 0 - viewHeight / 2, width: viewWidth, height: viewHeight))
        view.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
        view.isUserInteractionEnabled = false
          
        let envelopType = EnvelopPhase.top
        
        let bezierPath = envelopType.getShadowBezierPath(shadowWidth: 5, width: viewWidth, height: viewHeight)
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = bezierPath.cgPath
        
        view.layer.masksToBounds = false
        view.layer.mask = maskLayer
        
        view.layer.shadowPath = bezierPath.cgPath
        view.layer.shadowRadius = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOpacity = 0.3
        
        return view
    }()
    
    lazy private var bottomFlipView: UIView = {
        let viewHeight = (self.bounds.height)*35/100
        let viewWidth = self.bounds.width
        
        let view = UIView(frame: CGRect(x: 0, y: self.bounds.height - (viewHeight / 2), width: viewWidth, height: viewHeight))
        view.backgroundColor = self.envelopFlipViewBackgroundColor
        view.layer.anchorPoint = CGPoint(x: 0.5, y: 1)
        view.isUserInteractionEnabled = false
        
        let titlelabel = UILabel()
        titlelabel.text = "NeonMob"
        titlelabel.textColor = .black
        titlelabel.frame.size = CGSize(width: 60, height: 20)
        titlelabel.font = UIFont.systemFont(ofSize: 10, weight: .bold)
        titlelabel.center = CGPoint(x: viewWidth / 2, y: viewHeight / 2)
        titlelabel.alpha = 0.4
       // view.addSubview(titlelabel)
        let envelopType = EnvelopPhase.bottom

        view.applyBezierPath(path: envelopType.getBezeirPath(width: viewWidth, height: viewHeight))
        
        
        return view
    }()
    
    lazy private var leftFlipView: UIView = {
        let viewHeight = self.bounds.height
        let viewWidth = (self.bounds.width / 2) - 1
        
        let view = UIView(frame: CGRect(x: 0 - (viewWidth / 2), y: 0 , width: viewWidth, height: viewHeight))
        view.backgroundColor = self.envelopFlipViewBackgroundColor
        view.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
        view.isUserInteractionEnabled = false
        
        let envelopType = EnvelopPhase.left

        view.applyBezierPath(path: envelopType.getBezeirPath(width: viewWidth, height: viewHeight))
        
        return view
        
    }()
    
    lazy private var RightFlipView: UIView = {
        let viewHeight = self.bounds.height
        let viewWidth = (self.bounds.width / 2) - 1
        
        let view = UIView(frame: CGRect(x: self.bounds.width - (viewWidth / 2), y: 0 , width: viewWidth, height: viewHeight))
        view.backgroundColor = self.envelopFlipViewBackgroundColor
        view.layer.anchorPoint = CGPoint(x: 1, y: 0.5)
        view.isUserInteractionEnabled = false
        
        let envelopType = EnvelopPhase.right

        view.applyBezierPath(path: envelopType.getBezeirPath(width: viewWidth, height: viewHeight))
        
        return view
        
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initalSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
    private func initalSetup() {
        self.isUserInteractionEnabled = false
        
        self.backgroundColor = UIColor.clear
        
        self.addSubview(bottomFlipView)
        self.addSubview(leftFlipView)
        self.addSubview(RightFlipView)
        self.addSubview(topFlipShadowView)
        self.addSubview(topFlipView)

    }
    
    func openEnvelop() {
        
        var transform = CATransform3DIdentity;
        transform.m34 = 1.0 / 2000.0;
        
        UIView.animate(withDuration: 0.1) {
            self.topFlipShadowView.alpha = 0
        }
        
        UIView.animate(withDuration: 0.5,delay: 0.0,options: [.curveEaseOut],animations:  {
            
            self.topFlipView.layer.transform =  CATransform3DRotate(transform, CGFloat(-135 * Double.pi / 180), 1, 0, 0)
            self.bottomFlipView.layer.transform =  CATransform3DRotate(transform, CGFloat(-135 * Double.pi / 180), 1, 0, 0)
            self.leftFlipView.layer.transform = CATransform3DRotate(transform, CGFloat(-135 * Double.pi / 180), 0, 1, 0)
            self.RightFlipView.layer.transform = CATransform3DRotate(transform, CGFloat(-135 * Double.pi / 180), 0, 1, 0)
        })
        
    }
    
    func closeEnvelop() {
        UIView.animate(withDuration: 0.5,delay: 0.0,options: [.curveEaseOut],animations:  {
            
            self.topFlipView.transform = CGAffineTransform.identity
            self.bottomFlipView.transform = CGAffineTransform.identity
            self.leftFlipView.transform = CGAffineTransform.identity
            self.RightFlipView.transform = CGAffineTransform.identity
        }, completion: { _ in
            UIView.animate(withDuration: 0.2) {
                self.topFlipShadowView.alpha = 1
            }
        })
    }

}
