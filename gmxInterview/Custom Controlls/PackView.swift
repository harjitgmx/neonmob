//
//  PackView.swift
//  gmxInterview
//
//  Created by Harjit Singh on 01/11/19.
//  Copyright © 2019 gmx. All rights reserved.
//

import UIKit
import Foundation

class PackView: UIControl {
    
    private let height: CGFloat = 200
    private let width: CGFloat = 200
    
    private lazy var layerView: UIView = {
    
        let view = UIView(frame: self.bounds)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        return view
    }()
    
    override var isEnabled: Bool {
        didSet {
            isEnabled ? self.layerView.removeFromSuperview(): self.addSubview(layerView)
        }
    }
    
    var envelopView: EnvelopView?


    init(x: CGFloat, y: CGFloat) {
        super.init(frame: CGRect(x: x, y: y, width: width, height: height))
        
        initialSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func initialSetup() {
        
        // Adding Image view
        let imageView = UIImageView(frame: self.bounds)
        imageView.image = UIImage(named: "mcxco")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 5
        self.addSubview(imageView)
         
        self.envelopView = EnvelopView(frame: self.bounds)
        self.addSubview(envelopView!)
        
    }
    
    func openEnvelop() {
        self.envelopView?.openEnvelop()
    }
    
    
    func CloseEnvelop(){
        self.envelopView?.closeEnvelop()
    }
}
